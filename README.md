# Data Receiver

Data Receiver: receives a huge amount of data and stores in a data lake (e.g. AWS S3).

For the further information please refer to the platform [official documentation](https://gitlab.com/data-processing/data-processing-meta/-/blob/master/README.md)  
 
