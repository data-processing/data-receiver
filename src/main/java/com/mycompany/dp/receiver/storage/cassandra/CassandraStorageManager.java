package com.mycompany.dp.receiver.storage.cassandra;

import com.mycompany.dp.receiver.dto.EventRequestDto;
import com.mycompany.dp.receiver.storage.StorageManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Cassandra Storage manager to log event data.
 */
@Slf4j
@Service
@Profile("storage-cassandra")
public class CassandraStorageManager implements StorageManager {

  @Override
  public Mono<Void> logEvent(String requestId, EventRequestDto eventDto) {
    // TODO Here we should write the data by using cassandra reactive repository

    log.info("File with requestId {} successfully saved to Cassandra", requestId);

    return Mono.empty();
  }
}
