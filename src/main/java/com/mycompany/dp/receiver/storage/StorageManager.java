package com.mycompany.dp.receiver.storage;

import com.mycompany.dp.receiver.dto.EventRequestDto;
import reactor.core.publisher.Mono;

public interface StorageManager {

  Mono<Void> logEvent(String requestId, EventRequestDto eventDto);

}
