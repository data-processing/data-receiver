package com.mycompany.dp.receiver.storage.s3;

import static com.mycompany.dp.receiver.exception.ErrorEnum.FILE_INVALID_ERROR;
import static com.mycompany.dp.receiver.exception.ErrorEnum.FILE_UPLOAD_ERROR;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.GetBucketLocationRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.dp.receiver.dto.EventRequestDto;
import com.mycompany.dp.receiver.exception.BaseException;
import com.mycompany.dp.receiver.storage.StorageManager;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@Service
@Profile("storage-s3")
public class S3StorageManager implements StorageManager {

  private final AmazonS3 s3Client;
  private final ObjectMapper objectMapper;

  @Value("${receiver.storage.name:data-processing-storage}")
  private String bucketName;

  @Override
  public Mono<Void> logEvent(String requestId, EventRequestDto eventDto) {
    try {

      final byte[] fileContent = objectMapper.writeValueAsString(eventDto).getBytes();
      try (final InputStream fileStream = new ByteArrayInputStream(fileContent)) {
        // Create upload request
        final PutObjectRequest request = new PutObjectRequest(
            getBucketName(),
            getFileName(requestId, eventDto.getClickTime()),
            fileStream, getObjectMetadata(fileContent.length));

        request.withCannedAcl(CannedAccessControlList.AuthenticatedRead);

        // Upload to S3
        s3Client.putObject(request);
      }

      log.info("File with requestId {} successfully uploaded to Amazon S3", requestId);

      return Mono.empty();

    } catch (AmazonServiceException e) {
      log.error("Error occurred while trying to upload file to Amazon S3. Message {}", e.getErrorMessage());

      throw new BaseException(FILE_UPLOAD_ERROR);
    } catch (IOException e) {
      log.error("Unable to resolve multipart file. Message {}", e.getLocalizedMessage());

      throw new BaseException(FILE_INVALID_ERROR);
    }
  }

  private ObjectMetadata getObjectMetadata(int fileSize) {
    final ObjectMetadata metadata = new ObjectMetadata();
    metadata.setContentType(MediaType.APPLICATION_JSON.getType());
    metadata.setContentLength(fileSize);

    return metadata;
  }

  private String getBucketName() {
    if (!s3Client.doesBucketExistV2(bucketName)) {
      // Because the CreateBucketRequest object doesn't specify a region, the
      // bucket is created in the region specified in the client.
      s3Client.createBucket(new CreateBucketRequest(bucketName));

      // Verify that the bucket was created by retrieving it and checking its location.
      final String bucketLocation = s3Client.getBucketLocation(new GetBucketLocationRequest(bucketName));
      log.info("Bucket location: {}", bucketLocation);

    }

    return bucketName;
  }

  private String getFileName(String requestId, LocalDateTime now) {
    return String.format("%d-%d-%d-%d/%s",
        now.getYear(), now.getMonthValue(), now.getDayOfMonth(), now.getHour(), requestId);
  }
}
