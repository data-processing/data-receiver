package com.mycompany.dp.receiver.handler;

import com.mycompany.dp.receiver.dto.EventRequestDto;
import com.mycompany.dp.receiver.exception.ExceptionManager;
import com.mycompany.dp.receiver.service.EventService;
import com.mycompany.dp.receiver.validator.RequestValidator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * Takes event from route and logs by {@link EventService}.
 */
@Slf4j
@Component
@AllArgsConstructor
public class EventHandler {

  private final EventService eventService;
  private final RequestValidator requestValidator;

  /**
   * log user click request by {@link EventService}.
   *
   * @param serverRequest {@link ServerRequest}
   * @return {@link ServerResponse}
   */
  public Mono<ServerResponse> logEvent(ServerRequest serverRequest) {
    return serverRequest.bodyToMono(EventRequestDto.class)
        .doOnNext(requestValidator::validate)
        .flatMap(eventService::logEvent)
        .flatMap(nothing -> ServerResponse.noContent().build())
        .onErrorResume(ExceptionManager::toErrorResponse);
  }
}
