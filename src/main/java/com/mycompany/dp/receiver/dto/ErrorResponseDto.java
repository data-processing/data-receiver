package com.mycompany.dp.receiver.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Default error response representation.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorResponseDto {

  @Schema(description = "Error code.", example = "FIELD_VALIDATION_ERROR")
  private String errorCode;

  @Schema(description = "Error code.", example = "Validation failed on field(s)")
  private String errorMessage;

  @Schema(description = "List of validation errors.")
  private List<ValidationErrorResponseDto> validationErrors;

}
