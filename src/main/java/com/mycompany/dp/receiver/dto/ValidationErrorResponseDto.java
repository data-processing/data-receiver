package com.mycompany.dp.receiver.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidationErrorResponseDto {

  private String field;
  private String code;
  private String message;
  private Object rejectedValue;
}
