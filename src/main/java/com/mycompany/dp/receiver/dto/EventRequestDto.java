package com.mycompany.dp.receiver.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Request represantation of the user click.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventRequestDto {

  @Schema(description = "Email address of the user.",
      example = "jhon@smith.com", required = true)
  @Email
  @Size(max = 100)
  @NotBlank
  private String email;

  @Schema(description = "Name of the user.",
      example = "Jhon Smith")
  @Size(max = 255)
  private String name;

  @Schema(description = "Current screen of the user.",
      example = "Options", required = true)
  @Size(max = 100)
  @NotBlank
  private String screen;

  @Schema(description = "Timestamp of the click.",
      example = "2021-04-25T05:26:43.226Z", required = true)
  @NotNull
  @PastOrPresent
  private LocalDateTime clickTime;

}
