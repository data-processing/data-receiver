package com.mycompany.dp.receiver.route;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

import com.mycompany.dp.receiver.dto.EventRequestDto;
import com.mycompany.dp.receiver.handler.EventHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springdoc.core.annotations.RouterOperation;
import org.springdoc.core.annotations.RouterOperations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * Here are defined endpoints and swagger annotations for "/api/v1/event" base path.
 */
@Configuration
public class EventRoute {

  public static final String BASE_PATH = "/api/v1/event";
  public static final String USER_CLICK = "user-click";
  private static final String TAG = "EVENT";

  @RouterOperations( {
      @RouterOperation(path = BASE_PATH, method = RequestMethod.POST,
          operation = @Operation(operationId = USER_CLICK, tags = TAG,
              summary = "Receive user click event",
              requestBody = @RequestBody(content = @Content(schema =
              @Schema(implementation = EventRequestDto.class))),
              responses = {
                  @ApiResponse(responseCode = "204", description = "Event received"),
                  @ApiResponse(responseCode = "404", description = "User not found")
              }))
  })

  @Bean
  public RouterFunction<ServerResponse> userRouter(EventHandler handler) {
    return RouterFunctions
        .route(POST(BASE_PATH).and(accept(APPLICATION_JSON)), handler::logEvent);
  }
}
