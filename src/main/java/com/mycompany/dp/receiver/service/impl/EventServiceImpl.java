package com.mycompany.dp.receiver.service.impl;

import static com.mycompany.dp.receiver.utils.ReceiverConstants.EVENT_REQUEST_ID;

import com.mycompany.dp.receiver.dto.EventRequestDto;
import com.mycompany.dp.receiver.jms.JmsReceiverManager;
import com.mycompany.dp.receiver.service.EventService;
import com.mycompany.dp.receiver.storage.StorageManager;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
@AllArgsConstructor
public class EventServiceImpl implements EventService {

  private final StorageManager storageManager;
  private final JmsReceiverManager jmsManager;
  private final MeterRegistry meterRegistry;

  @Override
  public Mono<Void> logEvent(EventRequestDto eventDto) {
    log.info("Got event {}", eventDto);
    final String requestId = MDC.get(EVENT_REQUEST_ID);
    return storageManager.logEvent(requestId, eventDto)
        .and(jmsManager.sendEventToProcessor(requestId, eventDto))
        .doOnNext(a -> meterRegistry.counter("eventData.created", EVENT_REQUEST_ID,
            requestId).increment());
  }
}
