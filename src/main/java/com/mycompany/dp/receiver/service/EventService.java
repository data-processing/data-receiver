package com.mycompany.dp.receiver.service;

import com.mycompany.dp.receiver.dto.EventRequestDto;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface EventService {

  Mono<Void> logEvent(EventRequestDto eventDto);

}
