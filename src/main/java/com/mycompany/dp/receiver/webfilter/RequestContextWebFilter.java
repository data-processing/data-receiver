package com.mycompany.dp.receiver.webfilter;

import static com.mycompany.dp.receiver.utils.ReceiverConstants.EVENT_REQUEST_ID;

import java.util.List;
import java.util.UUID;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
public class RequestContextWebFilter implements WebFilter {

  @Override
  public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {

    final List<String> ids = serverWebExchange.getRequest().getHeaders().get(EVENT_REQUEST_ID);

    final String requestId = CollectionUtils.isEmpty(ids) ? UUID.randomUUID().toString() : ids.get(0);

    serverWebExchange.getResponse().getHeaders().add(EVENT_REQUEST_ID, requestId);

    MDC.put(EVENT_REQUEST_ID, requestId);

    return webFilterChain.filter(serverWebExchange);
  }
}
