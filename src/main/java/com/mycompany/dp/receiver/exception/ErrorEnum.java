package com.mycompany.dp.receiver.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Defines the error codes.
 */
@Getter
@AllArgsConstructor
public enum ErrorEnum {
  INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR,
      "We're having troubles, please try later"),
  FIELD_VALIDATION_ERROR(HttpStatus.PRECONDITION_FAILED, "Validation failed on field(s)"),
  FILE_UPLOAD_ERROR(HttpStatus.UNPROCESSABLE_ENTITY, "Unable to write event"),
  FILE_INVALID_ERROR(HttpStatus.UNPROCESSABLE_ENTITY, "Unable to write event");

  private final HttpStatus status;
  private final String message;

}
