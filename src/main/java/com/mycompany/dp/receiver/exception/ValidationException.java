package com.mycompany.dp.receiver.exception;

import com.mycompany.dp.receiver.dto.ValidationErrorResponseDto;
import java.util.List;
import lombok.Getter;

@Getter
public class ValidationException extends BaseException {

  private final transient List<ValidationErrorResponseDto> errorArgs;


  public ValidationException(ErrorEnum errorEnum, List<ValidationErrorResponseDto> errorArgs) {
    super(errorEnum);
    this.errorArgs = errorArgs;
  }

}
