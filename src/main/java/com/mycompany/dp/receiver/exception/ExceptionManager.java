package com.mycompany.dp.receiver.exception;

import static com.mycompany.dp.receiver.exception.ErrorEnum.INTERNAL_SERVER_ERROR;

import com.mycompany.dp.receiver.dto.ErrorResponseDto;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Slf4j
@UtilityClass
public class ExceptionManager {

  public Mono<ServerResponse> toErrorResponseFromBaseException(BaseException exception) {

    final ErrorResponseDto errorDto = ErrorResponseDto.builder()
        .errorCode(exception.getCode())
        .errorMessage(exception.getMessage())
        .build();

    if (exception instanceof ValidationException) {
      errorDto.setValidationErrors(((ValidationException) exception).getErrorArgs());
    }
    return ServerResponse
        .status(exception.getHttpStatus())
        .contentType(MediaType.APPLICATION_JSON)
        .bodyValue(errorDto);
  }

  public Mono<ServerResponse> toErrorResponse(Throwable throwable) {
    log.error("Exception during processing event: {}", throwable.getMessage());

    if (throwable instanceof BaseException) {
      return toErrorResponseFromBaseException((BaseException) throwable);
    }

    final ErrorResponseDto errorDto = ErrorResponseDto.builder()
        .errorCode(INTERNAL_SERVER_ERROR.toString())
        .errorMessage(throwable.getMessage())
        .build();

    return ServerResponse
        .status(INTERNAL_SERVER_ERROR.getStatus())
        .contentType(MediaType.APPLICATION_JSON)
        .bodyValue(errorDto);
  }
}
