package com.mycompany.dp.receiver.exception;

import org.springframework.http.HttpStatus;

/**
 * The base exception for the application.
 */
public class BaseException extends RuntimeException {

  private final ErrorEnum errorEnum;

  public BaseException(ErrorEnum errorEnum) {
    super(errorEnum.getMessage());
    this.errorEnum = errorEnum;
  }

  public HttpStatus getHttpStatus() {
    return errorEnum.getStatus();
  }

  public String getCode() {
    return errorEnum.toString();
  }

}
