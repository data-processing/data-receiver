package com.mycompany.dp.receiver.validator;

import static com.mycompany.dp.receiver.exception.ErrorEnum.FIELD_VALIDATION_ERROR;

import com.mycompany.dp.receiver.dto.ValidationErrorResponseDto;
import com.mycompany.dp.receiver.exception.ValidationException;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@AllArgsConstructor
public class RequestValidator {

  private final Validator validator;

  public void validate(Object target) {
    final Errors errors = new BeanPropertyBindingResult(target, target.getClass().getName());
    validator.validate(target, errors);
    if (errors.hasErrors()) {
      final List<ValidationErrorResponseDto> errorArgs = errors.getFieldErrors().stream()
          .map(error -> new ValidationErrorResponseDto(error.getField(), error.getCode(), error.getDefaultMessage(),
              error.getRejectedValue()))
          .collect(Collectors.toList());
      throw new ValidationException(FIELD_VALIDATION_ERROR, errorArgs);
    }
  }
}
