package com.mycompany.dp.receiver.jms;

import com.mycompany.dp.receiver.dto.EventRequestDto;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * POJO to send data by JMS to Data Processor.
 */
@Data
@NoArgsConstructor
public class EventPojo implements Serializable {

  static final long serialVersionUID = 1L;

  private String requestId;

  private String email;

  private String screen;

  private String name;

  private LocalDateTime clickTime;

  /**
   * Creates the {@link EventPojo}.
   *
   * @param requestId request id of event
   * @param requestDto original event from user
   * @return {@link EventPojo}
   */
  public static EventPojo from(String requestId, EventRequestDto requestDto) {
    final EventPojo eventPojo = new EventPojo();
    eventPojo.requestId = requestId;
    eventPojo.email = requestDto.getEmail();
    eventPojo.screen = requestDto.getScreen();
    eventPojo.name = requestDto.getName();
    eventPojo.clickTime = requestDto.getClickTime();

    return eventPojo;
  }

}
