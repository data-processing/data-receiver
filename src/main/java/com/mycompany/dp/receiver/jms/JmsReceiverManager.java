package com.mycompany.dp.receiver.jms;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.dp.receiver.dto.EventRequestDto;
import com.mycompany.dp.receiver.exception.BaseException;
import com.mycompany.dp.receiver.exception.ErrorEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@RequiredArgsConstructor
public class JmsReceiverManager {

  private final JmsTemplate jmsTemplate;
  private final ObjectMapper objectMapper;

  @Value("${receiver.processor.queueName:processor}")
  private String queueName;

  public Mono<Void> sendEventToProcessor(String requestId, EventRequestDto eventDto) {
    try {
      final EventPojo eventPojo = EventPojo.from(requestId, eventDto);

      log.info("Sending an event to processor: {}", eventPojo);

      jmsTemplate.convertAndSend(queueName, objectMapper.writeValueAsString(eventPojo));

      log.info("Successfully sent an event to processor: {}", eventPojo);

      return Mono.empty();
    } catch (Exception ex) {
      log.error("Exception occurred while sending event to analyzer. Message: {}", ex.getMessage());

      throw new BaseException(ErrorEnum.INTERNAL_SERVER_ERROR);
    }
  }
}
