package com.mycompany.dp.receiver.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
@Slf4j
public class MinioConfig {

  /**
   * We need this for minio as S3 provider. https://min.io/
   */
  @Bean
  public AmazonS3 amazonS3(AWSCredentialsProvider credentialsProvider,
      @Value("${receiver.minio.endpoint:http://localhost:9000}") String endpoint) {
    log.info("Minio starting at endpoint {}", endpoint);

    return AmazonS3ClientBuilder
        .standard()
        .withEndpointConfiguration(
            new AwsClientBuilder.EndpointConfiguration(endpoint, Regions.US_EAST_1.name()))
        .withPathStyleAccessEnabled(true)
        .withCredentials(credentialsProvider)
        .build();
  }
}
