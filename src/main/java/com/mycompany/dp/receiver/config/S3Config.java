package com.mycompany.dp.receiver.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("prod")
@Slf4j
public class S3Config {

  /**
   * AmazonS3 bean to store events {@link AmazonS3}
   */
  @Bean
  public AmazonS3 amazonS3(AWSCredentialsProvider credentialsProvider,
      @Value("${cloud.aws.region.static}") String region) {

    log.info("The region for aws s3 is {}", region);
    return AmazonS3ClientBuilder
        .standard()
        .withCredentials(credentialsProvider)
//        .withEndpointConfiguration(
//            new AwsClientBuilder.EndpointConfiguration("s3.amazonaws.com", region))
        .withRegion(region)
        .build();
  }

}
