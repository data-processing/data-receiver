package com.mycompany.dp.receiver.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Configuration WebFlux CORS properties.
 */
@Data
@Component
@ConfigurationProperties(prefix = "receiver.cors")
public class CorsProperties {

  private String mapping;
  private String[] allowedOrigins;
  private String[] allowedHeaders;
  private String[] allowedMethods;

}
