package com.mycompany.dp.receiver.config;

import com.mycompany.dp.receiver.config.properties.CorsProperties;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@EnableWebFlux
@Configuration
@AllArgsConstructor
public class WebFluxConfig implements WebFluxConfigurer {

  private final CorsProperties corsProperties;

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping(corsProperties.getMapping())
        .allowedOrigins(corsProperties.getAllowedOrigins())
        .allowedHeaders(corsProperties.getAllowedHeaders())
        .allowedMethods(corsProperties.getAllowedMethods())
        .exposedHeaders(HttpHeaders.SET_COOKIE);
  }
}