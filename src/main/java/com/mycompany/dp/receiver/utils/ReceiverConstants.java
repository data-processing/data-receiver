package com.mycompany.dp.receiver.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ReceiverConstants {

  public static final String EVENT_REQUEST_ID = "requestId";
}
