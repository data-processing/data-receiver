package com.mycompany.dp.receiver.route;

import static com.mycompany.dp.receiver.exception.ErrorEnum.FIELD_VALIDATION_ERROR;
import static com.mycompany.dp.receiver.exception.ErrorEnum.FILE_INVALID_ERROR;
import static com.mycompany.dp.receiver.route.EventRoute.USER_CLICK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.documentationConfiguration;

import com.mycompany.dp.receiver.ReceiverApplication;
import com.mycompany.dp.receiver.dto.ErrorResponseDto;
import com.mycompany.dp.receiver.dto.EventRequestDto;
import com.mycompany.dp.receiver.exception.BaseException;
import com.mycompany.dp.receiver.jms.JmsReceiverManager;
import com.mycompany.dp.receiver.service.EventService;
import com.mycompany.dp.receiver.storage.StorageManager;
import java.time.LocalDateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@ActiveProfiles( {"test"})
@SpringBootTest(classes = {ReceiverApplication.class})
public class EventRouteTest {

  @Rule
  public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

  @Autowired
  protected ApplicationContext context;
  protected WebTestClient webTestClient;

  @MockBean
  private StorageManager storageManager;

  @MockBean
  private JmsReceiverManager jmsManager;

  @SpyBean
  private EventService eventService;

  @Before
  public void setUp() {
    this.webTestClient = WebTestClient.bindToApplicationContext(this.context)
        .configureClient()
        .baseUrl("http://localhost:8011")
        .filter(documentationConfiguration(this.restDocumentation))
        .build();
  }

  @Test
  public void should_Fail_PostEvent_Validation() {
    final EventRequestDto requestDto = EventRequestDto.builder().build();

    webTestClient
        .post()
        .uri(EventRoute.BASE_PATH)
        .body(Mono.just(requestDto), EventRequestDto.class)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().is4xxClientError()
        .expectBody(ErrorResponseDto.class)
        .value(errorResponseDto -> {
              assertThat(errorResponseDto.getErrorCode())
                  .isEqualTo(FIELD_VALIDATION_ERROR.name());
              assertThat(errorResponseDto.getValidationErrors()).hasSize(3);
            }
        );

    then(eventService).should(never()).logEvent(any());
    then(storageManager).should(never()).logEvent(any(), any());
    then(jmsManager).should(never()).sendEventToProcessor(any(), any());
  }

  @Test
  public void should_Fail_PostEvent_FileInvalid() {

    final EventRequestDto requestDto = getEvent();

    given(storageManager.logEvent(any(), eq(requestDto))).willThrow(new BaseException(FILE_INVALID_ERROR));

    webTestClient
        .post()
        .uri(EventRoute.BASE_PATH)
        .body(Mono.just(requestDto), EventRequestDto.class)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().is4xxClientError()
        .expectBody(ErrorResponseDto.class)
        .value(errorResponseDto -> {
              assertThat(errorResponseDto.getErrorCode())
                  .isEqualTo(FILE_INVALID_ERROR.name());
              assertThat(errorResponseDto.getValidationErrors()).isNullOrEmpty();
            }
        );

    then(eventService).should().logEvent(eq(requestDto));
    then(storageManager).should().logEvent(any(), eq(requestDto));
    then(jmsManager).should(never()).sendEventToProcessor(any(), any());
  }

  @Test
  public void should_PostEvent() {
    given(storageManager.logEvent(any(), any())).willReturn(Mono.empty());
    given(jmsManager.sendEventToProcessor(any(), any())).willReturn(Mono.empty());

    final EventRequestDto requestDto = getEvent();

    webTestClient
        .post()
        .uri(EventRoute.BASE_PATH)
        .body(Mono.just(requestDto), EventRequestDto.class)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().is2xxSuccessful()
        .expectBody()
        .consumeWith(document(USER_CLICK, requestFields(
            fieldWithPath("email").description("Email address of the user"),
            fieldWithPath("name").description("Name of the user"),
            fieldWithPath("screen").description("Current screen of the user"),
            fieldWithPath("clickTime").description("Timestamp of the click")
        )));

    then(eventService).should().logEvent(eq(requestDto));
    then(storageManager).should().logEvent(any(), eq(requestDto));
    then(jmsManager).should().sendEventToProcessor(any(), eq(requestDto));
  }

  private static EventRequestDto getEvent() {
    return EventRequestDto.builder()
        .email("jhon@smith.com")
        .screen("Options")
        .name("Jhon Smith")
        .clickTime(LocalDateTime.now())
        .build();

  }
}
